<?php
header("Content-Type:application/json");

$AirSegments = kestfyhij('xml/057-6776453695.xml', 'AirSegments', 'Departure|Date|Time', 'Arrival|Date|Time', 'Board|Point', 'Off|Point');

$data = path_build($AirSegments);

$round_trip = implode('-',$data['round_trip']);
$endpoint = $data['endpoint'];
$cutpoint = $data['cutpoint'];

response($round_trip,$endpoint, $cutpoint,$AirSegments);

function path_build($data)
{
    $result = null;
    $date_arr = null;
    $result['cutpoint'] = false;
    $from = $data[0]['Board']['Point'];
    $result['round_trip'] = (array) $from;
    foreach($data as $key => $path)
    {
        $off = $data[$key]['Off']['Point'];
        $arrival = $data[$key]['Arrival']['Date'].' '.$data[$key]['Arrival']['Time'];
        $departure = $data[$key]['Departure']['Date'].' '.$data[$key]['Departure']['Time'];
        if(array_key_exists($key+1,$data))
        {
            $board = $data[$key+1]['Board']['Point'];
            $departure = $data[$key+1]['Departure']['Date'].' '.$data[$key+1]['Departure']['Time'];
        }
        else
        {
            $board = $from;
        }
    
        $date_arr[$board] = abs(strtotime($departure) - strtotime($arrival));
        $result['endpoint'] = array_keys($date_arr,max($date_arr))[0];

        if($off == $board)
        {
            $result['round_trip'][] = $board;
        } else {
            $result['round_trip'][] = $board;
            $result['cutpoint'] = $board;
        }
    }
    return $result;
}

function kestfyhij($content, $space, ... $scopes)
{
    $result = null;
    $count = 0;

    if (file_exists($content)) {
        $xml = simplexml_load_file($content);
    } else {
        $xml = simplexml_load_string($content);
    }

    foreach($xml->$space->children() as $loop)
    {
        foreach($scopes as $scope)
        {
            $attributes = explode('|', $scope);
            $scope = array_shift($attributes);
            foreach($attributes as $attribute)
            {
                $result[$count][$scope][$attribute] = $loop->$scope->attributes()->{$attribute}->__toString();
            }
        }
        $count++;
    }

    return $result;
}

// https://klisl.com/php-api-rest.html
function response($round_trip, $endpoint, $cutpoint, $AirSegments, $code = 200, $msg = true)
{
    echo json_encode([
        'status' => $msg, 
        'response_code' => $code, 
        'round_trip' => $round_trip, 
        'end_point'=> $endpoint,
        'cutpoint'=> $cutpoint,
        'all' => $AirSegments,
        'manual' => file_get_contents('info')
        ]);
}

?>
